# koha-api-docs

This project takes care of building the REST API docs for official publishing.

It includes the following releases

- 23.11 [default]
- 23.05
- 22.11
- 22.05
- 21.11
- 21.05
- 20.11
- 20.05
- 19.11

## Usage

If you want to host Koha's REST API documentation, you need to run it like this:

```shell
docker run --name koha-api-docs -d -p 8083:80 registry.gitlab.com/koha-community/koha-api-docs
```

See the _Packages & Registries_ section on the project for further guidance.

## Working locally

### Building

To build this image:

```shell
docker build . -t koha-api-docs
```

### Running

To run this image locally:

```shell
docker run --name koha-api-docs -d -p 8083:80 koha-api-docs
```

Note: this is making it listen on port _8083_, change it as required on your environment.

